FROM debian:wheezy

ENV DEBIAN_FRONTEND noninteractive
ENV DISPLAY :0.0

ARG URL_DOWNLOAD

RUN useradd -m -d /home/teamviewer -s /bin/bash teamviewer \
    && dpkg --add-architecture i386 \
    && apt-get update \
    && apt-get -y upgrade && apt-get -y dist-upgrade \
    && apt-get install -y wget ca-certificates \
    && wget ${URL_DOWNLOAD} -O /tmp/teamviewer \
    && apt-get -y install \
        sudo \
        libasound2:i386 \
        libexpat1:i386 \
        libfontconfig1:i386 \
        libfreetype6:i386 \
        libice6:i386 \
        libjpeg8:i386 \
        libpng12-0:i386 \
        libsm6:i386 \
        libx11-6:i386 \
        libxau6:i386 \
        libxcb1:i386 \
        libxdamage1:i386 \
        libxdmcp6:i386 \
        libxext6:i386 \
        libxfixes3:i386 \
        libxi6:i386 \
        libxrandr2:i386 \
        libxrender1:i386 \
        libxtst6:i386 \
        libjpeg62:i386 \
        libasound2 \
        libsm6 \
        libxfixes3 \
        libxinerama1:i386 \
        x11-common \
    && tar xf tmp/teamviewer \
    && if [ -d /teamviewer9 ]; then mv /teamviewer9 /teamviewer ; fi \
    && chown -R teamviewer: /teamviewer /home/teamviewer \
    && apt-get autoremove --purge -y \
    && apt-get clean \
    && rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/* \
    && echo "teamviewer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/teamviewer \
    &&  chmod 0440 /etc/sudoers.d/teamviewer

ADD run.sh /run.sh

USER teamviewer
ENV USER teamviewer

CMD /run.sh