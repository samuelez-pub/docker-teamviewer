# Teamviewer

`Dockerfile` to create a [Docker](https://www.docker.com/) image of
[teamviewer](http://teamviewer.com).

This container will attempt to mount the host's [X11](http://www.x.org) unix
domain socket in order to create its graphical window on the host's X11 server.
